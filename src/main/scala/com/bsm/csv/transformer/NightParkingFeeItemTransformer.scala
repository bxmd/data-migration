package com.bsm.csv.transformer

import java.time.LocalDate
import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}
import scalaz.std.list._
import scalaz.std.option._
import scalaz.syntax.traverse._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object NightParkingFeeItemTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filter(rawMap => {
            toDate(rawMap("tu_ngay"), "dd-MMM-uu")
                .plusDays(1)
                .isAfter(LocalDate.of(2018, 1, 1)) &&
                rawMap.get("nho_hon")
                    .flatMap(toMinMaxNum)
                    .isDefined
        })
        .mapConcat(rawMap => {
            val fee = extractFee(rawMap, "dongia", "vat", "dongiavat")
            val fee2 = extractFee(rawMap, "dongia2", "vat2", "dongiavat2")
            val fee3 = extractFee(rawMap, "dongia3", "vat3", "dongiavat3")

            val optionalFeeList = List(
               fee, fee2, fee3
            ).filter(_.isDefined).sequence

            optionalFeeList.getOrElse(List.empty)
        })
        .mapAsync(1)(rawMap =>
            for {
                seatTypeId <- rawMap.get("seat_type_code")
                    .map(askId(_, "seat_types"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve seat type id with code ${rawMap("seat_type_code")}")))
                nightParkingFeeId <- askId(
                    s"${rawMap.get("tu_ngay").map(toBsmDateString).getOrElse("null")}@${rawMap.get("den_ngay").map(toBsmDateString).getOrElse("null")}@$seatTypeId",
                    "night_parking_fees"
                )
            } yield {
                val (seatMin, seatMax) = toMinMaxNum(rawMap("nho_hon")).get
                UUID.randomUUID().toString ::
                    rawMap.getOrElse("dongia", "") ::
                    rawMap.getOrElse("dongiavat", "") ::
                    rawMap.getOrElse("vat", "") ::
                    seatMin ::
                    seatMax ::
                    nightParkingFeeId ::
                    "system" ::
                    "true" :: Nil
            }

        )

    override def newHeaderRow = List("id",
        "net_price",
        "gross_price",
        "vat",
        "seat_num_min",
        "seat_num_max",
        "night_parking_fee_id",
        "created_by",
        "in_used")

    transform()
}