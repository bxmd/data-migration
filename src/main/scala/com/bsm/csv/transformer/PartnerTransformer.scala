package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object PartnerTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filterNot(rawMap => rawMap("ma_dnvt") == "") //Remove record having partner-code empty
        .mapAsync(1)(rawMap =>
            for {
                cityId <- rawMap.get("ma_tinh")
                    .map(askId(_, "cities"))
                    .getOrElse(Future.successful(""))
            } yield {
                UUID.randomUUID().toString ::
                    rawMap.getOrElse("ma_dnvt", "") ::
                    rawMap.getOrElse("ten_dnvt", "") ::
                    rawMap.get("diachi_dnvt").map(fixSemicolon).getOrElse("") ::
                    rawMap.get("tel_dnvt").map(fixSemicolon).getOrElse("") ::
                    "" ::
                    rawMap.get("fax_dnvt").map(fixSemicolon).getOrElse("") ::
                    rawMap.getOrElse("giamdoc_dnvt", "") ::
                    rawMap.getOrElse("mst_dnvt", "") ::
                    (if (cityId == "") "null" else cityId) ::
                    "system" ::
                    "true" :: Nil
            }
        )

    override def newHeaderRow = List("id", "code", "name", "address", "phone_number_1", "phone_number_2", "fax_number", "owner_name", "tax_number", "city_id", "created_by", "in_used")

    transform()
}
