package com.bsm.csv.transformer

import java.time.LocalDate
import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}
import scalaz.std.list._
import scalaz.std.option._
import scalaz.syntax.traverse._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object MonthlyPaymentFeeTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filter(rawMap => {
            toDate(rawMap("tu_ngay"), "dd-MMM-uu")
                .plusDays(1)
                .isAfter(LocalDate.of(2018, 1, 1))
        })
        .mapConcat(rawMap => {
            val fee = extractFee(rawMap, "dongia", "vat", "dongiavat")
            val fee2 = extractFee(rawMap, "dongia2", "vat2", "dongiavat2")
            val fee3 = extractFee(rawMap, "dongia3", "vat3", "dongiavat3")
            val fee4 = extractFee(rawMap, "dongiaxk", "vatxk", "dongiavatxk")
            val fee5 = extractFee(rawMap, "dongiaxk2", "vatxk2", "dongiavatxk2")
            val fee6 = extractFee(rawMap, "dongiaxk3", "vatxk3", "dongiavatxk3")

            val optionalFeeList = List(
               fee, fee2, fee3, fee4, fee5, fee6
            ).filter(_.isDefined).sequence

            optionalFeeList.getOrElse(List.empty)
        })
        .mapAsync(1)(rawMap =>
            for {
                partnerId <- rawMap.get("ma_dnvt")
                    .map(askId(_, "partners"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve partner id with code ${rawMap("ma_dnvt")}")))
                routeId <- rawMap.get("ma_tuyen")
                    .map(askId(_, "routes"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve route id with code ${rawMap("ma_tuyen")}")))
                seatTypeId <- rawMap.get("seat_type_code")
                    .map(askId(_, "seat_types"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve seat type id with code ${rawMap("seat_type_code")}")))
            } yield {
                if (routeId == "") {
                    println(s"No route with code: ${rawMap("ma_tuyen")}")
                }
                UUID.randomUUID().toString ::
                    rawMap.get("tu_ngay").map(toBsmDateString).getOrElse("null") ::
                    rawMap.get("den_ngay").map(toBsmDateString).getOrElse("null") ::
                    rawMap.getOrElse("net_price", "") ::
                    rawMap.getOrElse("gross_price", "") ::
                    rawMap.getOrElse("vat", "") ::
                    toNullableId(partnerId) ::
                    toNullableId(routeId) :: //6
                    toNullableId(seatTypeId) ::
                    rawMap.getOrElse("trip_not_qualified", "") ::
                    "system" ::
                    "true" :: Nil
            }

        ).filter(_(6) != "null")

    override def newHeaderRow = List("id",
        "effective_date", //tungay
        "expiry_date", //denngay
        "net_price",
        "gross_price",
        "vat",
        "partner_id",
        "transport_route_id", //tuyenduong
        "seat_type_id",
        "trip_not_qualified",
        "created_by",
        "in_used")

    transform()
}