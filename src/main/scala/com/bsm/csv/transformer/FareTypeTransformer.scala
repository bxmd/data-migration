package com.bsm.csv.transformer

import java.nio.file.Paths
import java.util.UUID

import scala.concurrent.ExecutionContext.Implicits.global
import akka.stream.scaladsl.{FileIO, Source}
import com.bsm.csv
import com.bsm.csv.{IdResolvable, Validator}

object FareTypeTransformer extends App with Validator with IdResolvable {

    val newHeaderRow = List(
        "id",
        "code",
        "name",
        "seat_type_id",
        "in_used"
    )
    val (_, outputCsv, referenceCsvs) = validate()
    resolve(referenceCsvs.head) //resolve seat_types.csv as reference
    val sink = FileIO.toPath(Paths.get(outputCsv))
    Source(1 to 3)
        .flatMapConcat(i => {
            Source.apply(
                FareTypeCode.values.toList.map({ code =>
                    (
                        code.toString,
                        code.toString + s"$i",
                        code match {
                            case FareTypeCode.NGOI => s"Ghế ngồi loại $i"
                            case FareTypeCode.GHENAM => s"Ghế nằm loại $i"
                            case FareTypeCode.GIUONG => s"Giường nằm loại $i"
                        },
                        code match {
                            case FareTypeCode.NGOI => SeatType.NORMAL.toString
                            case FareTypeCode.GHENAM => SeatType.LYING.toString
                            case FareTypeCode.GIUONG => SeatType.BED.toString
                        }
                    )
                })
            )
        })
        .mapAsync(1)(codeDesc => {
            for {
                seatTypeId <- askId(codeDesc._4, "seat_types")
            } yield {
                UUID.randomUUID().toString ::
                    codeDesc._2 ::
                    codeDesc._3 ::
                    seatTypeId ::
                    "true" :: Nil
            }
        })
        .prefixAndTail(1)
        .flatMapConcat({ case (head, tail) =>
            Source(head).prepend(Source(List(newHeaderRow))).concat(tail)
        })
        .via(csv.getCSVFormatingFlow()).runWith(sink).onComplete(_ => system.terminate())
}