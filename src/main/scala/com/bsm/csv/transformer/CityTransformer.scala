package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global

object CityTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]].mapAsync(1)(rawMap =>
        for {
            regionId <- askId(rawMap.getOrElse("khu_vuc", ""), "regions")
        } yield {
            UUID.randomUUID().toString ::
                rawMap.getOrElse("ma_tinh", "") ::
                rawMap.getOrElse("ten_tinh", "") ::
                regionId ::
                "true" :: Nil
        }

    )

    override def newHeaderRow = List("id", "code", "name", "region_id", "in_used")

    transform()
}
