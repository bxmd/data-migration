package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TripArrivalTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filter(rawMap => rawMap.get("ngayvaoben").exists(_.endsWith("/2018")))
        .mapAsync(1)(rawMap =>
            for {
                gateId <- askId("G001", "gates")
                arrivalTime <- Future.successful(toBsmDateTime(rawMap.get("ngayvaoben"), rawMap.get("giovaoben")).getOrElse(""))
            } yield {
                UUID.randomUUID().toString ::
                    arrivalTime ::
                    gateId ::
                    rawMap.getOrElse("baovenhap", "") ::
                    "" ::
                    rawMap.getOrElse("biensoxe", "") ::
                    "" ::
                    "system" ::
                    "true" ::
                    "true" :: Nil
            }
        )

    override def newHeaderRow = List("id", "arrival_time", "gate_id", "checked_in_by", "notes", "vehicle_license_number", "card_series_number", "created_by", "processed_for_trip_assignment","in_used")

    transform()
}