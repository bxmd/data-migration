package com.bsm.csv.transformer

import java.nio.file.Paths
import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.scaladsl.{FileIO, Source}
import akka.stream.{ActorMaterializer, Materializer}
import com.bsm.csv

import scala.concurrent.ExecutionContext.Implicits.global

object VehicleTypeGenerator extends App {

    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: Materializer = ActorMaterializer()

    val newHeaderRow = List(
        "id",
        "code",
        "name",
        "in_used"
    )
    val sink = FileIO.toPath(Paths.get(s"${args(0)}"))
    Source(List(("NOIBO", "Xe thuộc bến"), ("TAI", "Xe tải"), ("KHACHVANGLAI", "Xe khách vãng lai")))
        .map(it => UUID.randomUUID().toString ::
            s"${it._1}" ::
            s"${it._2}" ::
            "true" :: Nil
        )
        .prefixAndTail(1)
        .flatMapConcat({ case (head, tail) =>
            Source(head).prepend(Source(List(newHeaderRow))).concat(tail)
        })
        .via(csv.getCSVFormatingFlow()).runWith(sink).onComplete(_ => system.terminate())
}
