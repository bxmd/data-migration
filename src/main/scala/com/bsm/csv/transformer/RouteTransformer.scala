package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global

object RouteTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]].mapAsync(1)(rawMap =>
        for {
            toCityId <- askId(rawMap.getOrElse("ma_tinh", ""), "cities")
            hcmId <- askId("HCM", "cities")
        } yield {
            UUID.randomUUID().toString ::
                rawMap.getOrElse("ma_tuyen", "") ::
                rawMap.getOrElse("ten_tuyen", "") ::
                "null" ::
                rawMap.getOrElse("culy_tuyen", "") ::
                hcmId ::
                toCityId ::
                "system" ::
                "true" :: Nil
        }

    )

    override def newHeaderRow = List("id", "code", "name", "stops", "distance", "from_city_id", "to_city_id", "created_by", "in_used")

    transform()
}