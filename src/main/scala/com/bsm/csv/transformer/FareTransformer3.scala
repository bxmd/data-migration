package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scalaz.syntax.traverse._
import scalaz.std.list._
import scalaz.std.option._

object FareTransformer3 extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filterNot(rawMap => rawMap("gia_loai3") == "0" || rawMap("gia_loai3") == "")
        .map(rawMap => {
            println(s"DNVT : $rawMap")
            var loaighe = rawMap("maloaive")
            var loaive  = "3";
            rawMap.updated("maloaive", fromSeatType(loaighe,3))
        })
        .mapAsync(1)(rawMap =>
            for {
                partnerId <- rawMap.get("ma_dnvt")
                    .map(askId(_, "partners"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve partner id with code ${rawMap("ma_dnvt")}")))
                cityId <- rawMap.get("ma_tinh")
                    .map(askId(_, "cities"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve city id with code ${rawMap("ma_tinh")}")))
                routeId <- rawMap.get("tuyenduong")
                    .map(askId(_, "routes"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve route id with code ${rawMap("tuyenduong")}")))
                fareTypeId <- rawMap.get("maloaive")
                    .map(askId(_, "fare_types"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve fare type id with code ${rawMap("maloaive")}")))
            } yield { UUID.randomUUID().toString ::
                    rawMap.get("tungay").getOrElse("null") ::
                    rawMap.get("denngay").getOrElse("null") ::
                    (if (partnerId == "") "null" else partnerId) ::
                    (if (cityId == "") "null" else cityId) ::
                    (if (routeId == "") "null" else routeId) ::
                    (if (fareTypeId == "") "null" else fareTypeId) ::
                    rawMap.get("uythac").map(toBsmBoolean).getOrElse("null") ::
                    rawMap("gia_loai3") ::
                    "system" ::
                    "true" :: Nil
            }

        )

    override def newHeaderRow = List("id",
        "effective_date", //tungay
        "expiry_date", //denngay
        "partner_id", //ma_dnvt
        "city_id", //ma_tinh
        "transport_route_id", //tuyenduong
        "fare_type_id", //maloaive
        "ticket_sale_delegation", //uythac
        "fare", //gia
        "created_by",
        "in_used")

    transform()
}