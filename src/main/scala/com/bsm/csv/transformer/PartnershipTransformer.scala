package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object PartnershipTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filterNot(rawMap => rawMap("ma_dnvt") == "") //Remove record having partner-code empty
        //.filterNot(rawMap => rawMap("ngunghd") == "TRUE")
        .mapAsync(1)(rawMap =>
            for {
                partnerId <- rawMap.get("ma_dnvt")
                    .map(askId(_, "partners"))
                    .getOrElse(Future.failed(new RuntimeException(s"There is no partner with code ${rawMap("ma_dnvt")}")))
            } yield UUID.randomUUID().toString ::
                rawMap.get("ngayngunghd").map(toBsmDateString).getOrElse("null") ::
                partnerId ::
                rawMap.get("ngunghd").map(toPartnerStatus(_).toString).getOrElse("ACTIVE") ::
                "system" ::
                "true" :: Nil
        )

    override def newHeaderRow = List("id", "effective_date", "partner_id", "partner_ship_status", "created_by", "in_used")

    transform()
}