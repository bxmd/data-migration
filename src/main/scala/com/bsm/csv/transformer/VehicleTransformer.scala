package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object VehicleTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filterNot(_.getOrElse("bienso", "").isEmpty)
        //.filterNot( rawMap => rawMap("ngunghd") == "TRUE")
        .mapAsync(1)(rawMap =>
            for {
                partnerId <- rawMap.get("ma_dnvt")
                    .map(askId(_, "partners"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve partner id with code ${rawMap("ma_dnvt")}")))
                ticketBoothId <- rawMap.get("quayve")
                    .map(askId(_, "ticket_booths"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve ticket booth id with code ${rawMap("quayve")}")))
                primaryRouteId <- askId(rawMap("ma_tuyenchinh"), "routes")
                secondaryRouteId <- rawMap.get("ma_tuyenphu")
                    .map(askId(_, "routes"))
                    .getOrElse(Future.successful(""))
                fareTypeId <- rawMap.get("loaive").map(toFareTypeCode)
                    .map(askId(_, "fare_types"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve fare type id with code ${rawMap("loaive")}")))
                seatTypeId <- rawMap.get("loaive").map(toSeatType)
                    .map(askId(_, "seat_types"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve seat type id with code ${rawMap("loaive")}")))
            } yield {
                UUID.randomUUID().toString ::
                    rawMap.getOrElse("bienso", "") ::
                    (if (seatTypeId == null) "null" else seatTypeId) ::
                    rawMap.getOrElse("soghe", "") ::
                    rawMap.getOrElse("soghemax", "") ::
                    primaryRouteId ::
                    (if (secondaryRouteId == "") "null" else secondaryRouteId) ::
                    rawMap.get("uythac").map(toBsmBoolean).getOrElse("") ::
                    rawMap.get("tel_taixe").map(fixSemicolon).getOrElse("") ::
                    "null" ::
                    rawMap.get("kiemdinh").getOrElse("null") ::
                    rawMap.get("baohiem").getOrElse("null") ::
                    rawMap.get("tuyen_cd").getOrElse("null") ::
                    rawMap.get("camco").getOrElse("null") ::
                    "" ::
                    rawMap.getOrElse("hieuxe", "") ::
                    (if (partnerId == "") "null" else partnerId) ::
                    (if (ticketBoothId == "") "null" else ticketBoothId) ::
                    (if (fareTypeId == null) "null" else  fareTypeId) ::
                    "system" ::
                    "true" :: Nil
            }
    )

    override def newHeaderRow = List("id",
        "license_number", //bienso
        "seat_type_id",
        "total_seats", //soghe,
        "total_seatable", //soghemax
        "primary_route_id", //ma_tuyenchinh
        "secondary_route_id", //ma_tuyenphu
        "ticket_sale_delegation", //uythac
        "contact_number", //tel_taixe
        "expiration_date", //ngay_hethan
        "register_expiry_date", //kiemdinh
        "insurance_expiry_date", //baohiem,
        "fixed_route_expiry_date", //tuyen_cd
        "maturity_date", //camco
        "notes", //new field
        "vehicle_model", //new field
        "partner_id", //ma_dnvt
        "ticket_booth_id", //quayve
        "fare_type_id",
        "created_by",
        "in_used")

    transform()
}
