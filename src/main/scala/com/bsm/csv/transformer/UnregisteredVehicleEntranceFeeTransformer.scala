package com.bsm.csv.transformer

import java.time.LocalDate
import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object UnregisteredVehicleEntranceFeeTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filter(rawMap => {
            toDate(rawMap("tu_ngay"), "dd-MMM-uu")
                .plusDays(1)
                .isAfter(LocalDate.of(2018, 1, 1))
        })
        .mapAsync(1)(rawMap =>
            for {
                vehicleTypeId <- rawMap.get("loaixe").map(toVehicleTypeCode)
                    .map(askId(_, "vehicle_types"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve vehicle type id with code ${rawMap.get("loaixe").map(toVehicleTypeCode)}")))
            } yield {
                UUID.randomUUID().toString ::
                    rawMap.get("tu_ngay").map(toBsmDateString).getOrElse("null") ::
                    rawMap.get("den_ngay").map(toBsmDateString).getOrElse("null") ::
                    rawMap.getOrElse("dongia", "") ::
                    rawMap.getOrElse("dongiavat", "") ::
                    rawMap.getOrElse("vat", "") ::
                    toNullableId(vehicleTypeId) ::
                    "system" ::
                    "true" :: Nil
            }

        ).filter(_(6) != "null")

    override def newHeaderRow = List("id",
        "effective_date", //tungay
        "expiry_date", //denngay
        "net_price",
        "gross_price",
        "vat",
        "vehicle_type_id",
        "created_by",
        "in_used")

    transform()
}