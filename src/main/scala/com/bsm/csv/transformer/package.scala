package com.bsm.csv

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

import akka.actor.{ActorRef, ActorSystem}
import akka.util.Timeout
import com.bsm.csv.actor.AckingReceiver.GetId

import scala.annotation.tailrec
import scala.collection.immutable.::
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationLong
import scala.util.{Failure, Try}

package object transformer {
    val dateFormats = List(
        "uuuu/MM/dd",
        "MM/dd/uuuu",
        "MMM dd, uuuu",
        "dd MMMM uuuu",
        "dd MMM uuuu",
        "dd-MM-uuuu",
        "dd-MMM-uu"
    ).map(p => (p, DateTimeFormatter.ofPattern(p)))

    def normalizeDate(dateStr: String, toPattern: String): Option[String] = {
        val trimmedDate = dateStr.trim

        @tailrec
        def normalize(patterns: List[(String, DateTimeFormatter)]): Try[TemporalAccessor] = patterns match {
            case head :: tail =>
                val resultTry = Try(head._2.parse(trimmedDate))
                if (resultTry.isSuccess) resultTry else normalize(tail)
            case _ => Failure(new RuntimeException("no match found"))
        }

        if (trimmedDate.isEmpty) None
        else normalize(dateFormats).map(DateTimeFormatter.ofPattern(toPattern).format).toOption
    }

    def toBsmDateString(legacyDate: String) = normalizeDate(legacyDate, "uuuu-MM-dd").getOrElse("null")

    def toDate(dateString: String, pattern: String) = {
        import java.time.LocalDate
        import java.time.format.DateTimeFormatter
        val formatter = DateTimeFormatter.ofPattern(pattern)

        //convert String to LocalDate
        LocalDate.parse(dateString, formatter)
    }

    def toBsmDateTime(legacyDate: Option[String], legacyTime: Option[String]) = {
        for {
            date <- legacyDate.flatMap(normalizeDate(_, "uuuu-MM-dd"))
            time <- legacyTime
        } yield s"${date}T$time"
    }

    def toBsmDateddMMMuu(legacyDate: String) = normalizeDate(legacyDate, "dd-MMM-uu").getOrElse("null")

    object SeatType extends Enumeration {
        type SeatType = Value
        val NORMAL, LYING, BED = Value
    }

    object FareTypeCode extends Enumeration {
        type FareTypeCode = Value
        val NGOI, GHENAM, GIUONG = Value
    }
    def toBsmExpiryDate(dateString: String) = {
        val expiryDate = toDate(dateString, "MM/dd/uuuu")
        if (expiryDate.isAfter(LocalDate.now())) "null" else dateString
    }

    def toFareTypeCode(maloaive: String, loaive: Int): Option[String] = {
        val idx = maloaive.toInt - 1
        if (idx < 0 || idx > 2) {
            None
        } else {
            Some(s"${SeatType.apply(idx).toString}$loaive")
        }
    }
    /**
    ** Ngoi1,2,3 --> Ngoi1,2,3
    *  Giuong, Giuong 1 --> Giuong 3 (Gia thap nhat, 45 cho)
    *  Giuong, Giuong 2 --> Giuong 1 (Gia cao nhat, 25 cho)
    *  Giuong3 --> Giuong 2 (30 cho)
    *  Ghe hien tai chi co mot loai  
    */
    def toFareTypeCode(loaive: String): String = {
        if (loaive == "NGOI1") "NGOI1"
        else if (loaive == "NGOI2") "NGOI2"
        else if (loaive == "NGOI3") "NGOI3"
        else if (loaive == "GIUONG") "GIUONG3"
        else if (loaive == "GIUONG1") "GIUONG3"
        else if (loaive == "GIUONG2") "GIUONG1"
        else if (loaive == "GIUONG3") "GIUONG2"
        else if (loaive == "GHENAM") "GHENAM1"
        else "NGOI1"
    }

    def toSeatType(loaive: String) = {
        if (loaive == "NGOI1") "NORMAL"
        else if (loaive == "NGOI2") "NORMAL"
        else if (loaive == "NGOI3") "NORMAL"
        else if (loaive == "GIUONG") "BED"
        else if (loaive == "GIUONG1") "BED"
        else if (loaive == "GIUONG2") "BED"
        else if (loaive == "GIUONG3") "BED"
        else if (loaive == "GHENAM") "LYING"
        else "NORMAL"
    } 
    /**
    ** Ngoi1,2,3 --> Ngoi1,2,3
    *  Giuong, Giuong 1 --> Giuong 3 (Gia thap nhat, 45 cho)
    *  Giuong, Giuong 2 --> Giuong 1 (Gia cao nhat, 25 cho)
    *  Giuong3 --> Giuong 2 (30 cho)
    *  Ghe hien tai chi co mot loai  
    */
    def fromSeatType(loaighe: String, loaive: Int) = {
        if (loaighe == "1") { // Ngoi
            if (loaive == 1) "NGOI1"
            else if (loaive == 2) "NGOI2"
            else if (loaive == 3) "NGOI3"
            else ""
        } else if (loaighe == "2") { // Ghe nam
            if (loaive == 1) "GHENAM1"
            else if (loaive == 2) "GHENAM2"
            else if (loaive == 3) "GHENAM3"
            else ""
        } else if (loaighe == "3") { // Giuong
            if (loaive == 1) "GIUONG3"
            else if (loaive == 2) "GIUONG1"
            else if (loaive == 3) "GIUONG2"
            else ""
        }  else ""
    }

    object PartnerStatus extends Enumeration {
        type PartnerStatus = Value
        val ACTIVE, INACTIVE = Value
    }

    def toPartnerStatus(isInactivity: String) =
        if (isInactivity == "T" || isInactivity == "TRUE") PartnerStatus.INACTIVE
        else if (isInactivity == "F" || isInactivity == "FALSE") PartnerStatus.ACTIVE
        else "null"

    def toBsmBoolean(legacyBoolean: String) =
        if (legacyBoolean == "T" || legacyBoolean == "TRUE") "true"
        else if (legacyBoolean == "F" || legacyBoolean == "FALSE") "false"
        else ""

    def toNullableId(id: String) = if (id == "") "null" else id

    def askId(code: String, actorName: String)(implicit system: ActorSystem, askTimeout: Timeout) = {
        import akka.pattern.ask
        val futIdResolver = system.actorSelection(system.child(s"$actorName")).resolveOne()
        for {
            idResolver <- futIdResolver
            result <- (idResolver ? GetId(code)).mapTo[String]
        } yield result
    }

    def checkVehicleModelDuplicate(modelName: String, yearOfManufacture: String, dupChecker: ActorRef) = {
        import akka.pattern.ask
        implicit val askTimeout = Timeout(30 seconds)
        (dupChecker ? s"$modelName@$yearOfManufacture").mapTo[Boolean]
    }

    def checkNightParkingDuplicate(fromDate: String, toDate: String, dupChecker: ActorRef) = {
        import akka.pattern.ask
        implicit val askTimeout = Timeout(30 seconds)
        (dupChecker ? s"$fromDate@$toDate").mapTo[Boolean]
    }

    def fixSemicolon(text: String) = text.replace(';', ',')

    def extractFee(rawRecord: Map[String, String],
                   netPriceColName: String,
                   vatColName: String,
                   grossPriceColName: String): Option[Map[String, String]] = {
        val seatType: Option[SeatType.SeatType] = netPriceColName match {
            case "dongia" | "dongiaxk" => Some(SeatType.NORMAL)
            case "dongia2" | "dongiaxk2" => Some(SeatType.LYING)
            case "dongia3" | "dongiaxk3" => Some(SeatType.BED)
            case _ => None
        }

        val tripNotQualified: Option[Boolean] = netPriceColName match {
            case "dongia" | "dongia2" | "dongia3" => Some(false)
            case "dongiaxk" | "dongiaxk2" | "dongiaxk3" => Some(true)
            case _ => None
        }

        for {
            st <- seatType
            tq <- tripNotQualified
            np <- rawRecord.get(netPriceColName)
            vat <- rawRecord.get(vatColName)
            gp <- rawRecord.get(grossPriceColName)
        } yield {
            rawRecord.updated("net_price", np)
                .updated("vat", vat)
                .updated("gross_price", gp)
                .updated("seat_type_code", st.toString)
                .updated("trip_not_qualified", tq.toString)
        }
    }

    def toVehicleTypeCode(loaixe: String) = {
        if (loaixe == "1") "NOIBO"
        else if (loaixe == "2") "TAI"
        else if (loaixe == "3") "KHACHVANGLAI"
        else "KHACHVANGLAI"
    }

    def toMinMaxNum(nhohon: String): Option[(String, String)] = {
        if (nhohon == "15") {
            Some(("0", "14"))
        } else if (nhohon == "25") {
            Some(("15", "24"))
        } else if (nhohon == "35") {
            Some(("25", "34"))
        } else if (nhohon == "100") {
            Some(("35", "99"))
        } else {
            None
        }
    }

}
