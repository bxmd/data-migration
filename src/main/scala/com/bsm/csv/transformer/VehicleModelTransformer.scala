package com.bsm.csv.transformer

import java.util.UUID

import akka.actor.Props
import akka.stream.scaladsl.Flow
import com.bsm.csv.actor.DuplicateChecker
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global

object VehicleModelTransformer extends App with Validator with Transformer {

    val vehicleModelDuplicateChecker = system.actorOf(Props[DuplicateChecker], "vehicleModelDuplicateChecker")

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .mapAsync(1)(rawMap =>
            checkVehicleModelDuplicate(
                rawMap.getOrElse("hieuxe", ""),
                rawMap.getOrElse("namsx", ""),
                vehicleModelDuplicateChecker
            ).map((_, rawMap)))
        .filterNot(newRowMap => newRowMap._1 || newRowMap._2.getOrElse("hieuxe", "").trim.isEmpty)
        .map(newRowMap =>
            UUID.randomUUID().toString ::
                s"${newRowMap._2.getOrElse("hieuxe", "")}@${newRowMap._2.getOrElse("namsx", "")}" ::
                newRowMap._2.getOrElse("hieuxe", "") ::
                newRowMap._2.getOrElse("namsx", "") ::
                "null" ::
                "true" :: Nil
        )

    override def newHeaderRow = List("id",
        "code",
        "model_name", //hieuxe
        "manufactured_year", //namsx
        "vehicle_type",
        "in_used")

    transform()
}
