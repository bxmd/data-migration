package com.bsm.csv.transformer

import java.nio.file.Paths
import java.util.UUID

import scala.concurrent.ExecutionContext.Implicits.global
import akka.stream.scaladsl.{FileIO, Source}
import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}


object FareTypeTransformer_New extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]].mapAsync(1)(rawMap =>
        for {
            seat_type_id <- askId(rawMap.getOrElse("loaighe", ""), "seat_types")
        } yield {
            UUID.randomUUID().toString ::
                rawMap.getOrElse("maloaive", "") ::
                rawMap.getOrElse("diengiai", "") ::
                seat_type_id ::
                "true" :: Nil
        }

    )
    override def newHeaderRow = List(
        "id",
        "code",
        "name",
        "seat_type_id",
        "in_used"
    )
}