package com.bsm.csv.transformer

import java.time.LocalDate
import java.util.UUID

import akka.actor.Props
import akka.stream.scaladsl.Flow
import com.bsm.csv.actor.DuplicateChecker
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object NightParkingFeeTransformer extends App with Validator with Transformer {

    val nightParkingDuplicateChecker = system.actorOf(Props[DuplicateChecker], "nightParkingDuplicateChecker")

    override def buildTransformerFlow() = Flow[Map[String, String]]
        .filter(rawMap => {
            toDate(rawMap("tu_ngay"), "dd-MMM-uu")
                .plusDays(1)
                .isAfter(LocalDate.of(2018, 1, 1)) &&
                rawMap.get("nho_hon")
                    .flatMap(toMinMaxNum)
                    .isDefined
        })
        .mapAsync(1)(rawMap =>
            checkVehicleModelDuplicate(
                rawMap.getOrElse("tu_ngay", ""),
                rawMap.getOrElse("den_ngay", ""),
                nightParkingDuplicateChecker
            ).map(d => if (d) List.empty[Map[String,String]] else List(rawMap))
        )
        .filterNot(_.isEmpty)
        .mapConcat(rawMapList => {
            List(
                rawMapList.head.updated("seat_type_code", SeatType.NORMAL.toString),
                rawMapList.head.updated("seat_type_code", SeatType.LYING.toString),
                rawMapList.head.updated("seat_type_code", SeatType.BED.toString)
            )
        })
        .mapAsync(1)(rawMap =>
            for {
                seatTypeId <- rawMap.get("seat_type_code")
                    .map(askId(_, "seat_types"))
                    .getOrElse(Future.failed(new RuntimeException(s"Can not resolve seat type id with code ${rawMap("seat_type_code")}")))
            } yield {
                UUID.randomUUID().toString ::
                    rawMap.get("tu_ngay").map(toBsmDateString).getOrElse("null") ::
                    rawMap.get("den_ngay").map(toBsmDateString).getOrElse("null") ::
                    toNullableId(seatTypeId) ::
                    "system" ::
                    "true" :: Nil
            }

        )

    override def newHeaderRow = List("id",
        "effective_date", //tungay
        "expiry_date", //denngay
        "seat_type_id",
        "created_by",
        "in_used")

    transform()
}