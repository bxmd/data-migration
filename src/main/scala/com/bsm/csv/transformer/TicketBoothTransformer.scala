package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

object TicketBoothTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]].map(rawMap =>
            UUID.randomUUID().toString ::
            rawMap.getOrElse("maquayve", "") ::
            rawMap.getOrElse("diengiai", "") ::
            "true" :: Nil
    )

    override def newHeaderRow = List("id", "code", "name", "in_used")

    transform()
}