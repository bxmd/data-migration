package com.bsm.csv.transformer

import java.util.UUID

import akka.stream.scaladsl.Flow
import com.bsm.csv.{Transformer, Validator}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TimetableTransformer extends App with Validator with Transformer {

    override def buildTransformerFlow() = Flow[Map[String, String]].mapAsync(1)(rawMap =>
        for {
            routeId <- rawMap.get("ma_tuyen")
                .map(askId(_, "routes"))
                .getOrElse(Future.failed(new RuntimeException(s"Can not resolve id of route with code=${rawMap("ma_tuyen")}")))
            partnerId <- rawMap.get("ma_dnvt")
                .map(askId(_, "partners"))
                .getOrElse(Future.failed(new RuntimeException(s"Can not resolve id of partner with code=${rawMap("ma_dnvt")}")))
        } yield {
            UUID.randomUUID().toString ::
                rawMap("tai_so") ::
                rawMap("gio_chay") ::
                rawMap.get("ngay_chay").map(fixSemicolon).getOrElse("") ::
                rawMap.getOrElse("songaychay", "") ::
                rawMap.getOrElse("tongchuyen", "") ::
                (if (routeId == "") "null" else routeId) ::
                (if (partnerId == "") "null" else partnerId) ::
                "system" ::
                "true" :: Nil
        }
    )

    override def newHeaderRow = List("id",
        "slot_number", //tai_so
        "departure_time", //gio_chay
        "routine_type", //ngay_chay,
        "number_of_days", //songaychay
        "number_of_trip", //tongchuyen
        "route_id", //ma_tuyen
        "partner_id", //ma_dnvt
        "created_by",
        "in_used")

    transform()
}