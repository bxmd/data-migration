package com.bsm.csv

import java.nio.file.Paths

import akka.NotUsed
import akka.event.Logging
import akka.stream.Attributes
import akka.stream.alpakka.csv.scaladsl.CsvToMap
import akka.stream.scaladsl.{FileIO, Flow, Keep, Source}
import akka.util.ByteString

import scala.concurrent.ExecutionContext.Implicits.global

trait Transformer extends IdResolvable { self: Validator =>

    def buildTransformerFlow(): Flow[Map[String, String], List[String], NotUsed]

    def newHeaderRow: List[String]

    def transform() = {

        val (csvInput, csvOutput, csvReferences) = validate()

        csvReferences.foreach( references =>{
            resolve(references)
        })

        val transformerGraph = buildTransformerGraph(csvInput, csvOutput)
        transformerGraph.run().onComplete(_ => system.terminate())
    }

    def buildTransformerGraph(csvInput: String,
                              csvOutput: String) = {

        val source = FileIO.fromPath(Paths.get(s"$csvInput")).map(_.utf8String.replace('\\', '\0')).map(ByteString(_))

        val transformer = getCSVParsingFlow()
            .via(CsvToMap.toMapAsStrings())
            .via(buildTransformerFlow())
            .prefixAndTail(1)
            .flatMapConcat({ case (head, tail) =>
                Source(head).prepend(Source(List(newHeaderRow))).concat(tail)
            })
            .log("before-store")
            .withAttributes(Attributes.logLevels(onElement = Logging.DebugLevel))
            .via(getCSVFormatingFlow())

        val sink = FileIO.toPath(Paths.get(s"$csvOutput"))

        source.via(transformer).toMat(sink)(Keep.right)
    }
}
