package com.bsm

import java.io.File
import java.nio.charset.StandardCharsets

import akka.stream.alpakka.csv.scaladsl.{CsvFormatting, CsvParsing, CsvQuotingStyle}

package object csv {
    def getCSVParsingFlow(useDelimiter: Byte = CsvParsing.Comma) = {
        import CsvParsing.{Backslash, DoubleQuote}
        val quoteCharParsing: Byte = DoubleQuote
        val escapeCharParsing: Byte = Backslash

        import akka.stream.alpakka.csv.scaladsl.CsvParsing
        CsvParsing.lineScanner(useDelimiter, quoteCharParsing, escapeCharParsing)
    }

    def getCSVFormatingFlow() = {
        import CsvFormatting.{Backslash, DoubleQuote, SemiColon}
        val delimiter = SemiColon
        val quoteChar = DoubleQuote
        val escapeChar = Backslash
        val endOfLine = "\r\n"
        CsvFormatting.format(delimiter, quoteChar, escapeChar, endOfLine, CsvQuotingStyle.Required, StandardCharsets.UTF_8, None)
    }

    def fileNameWithoutExtension(fileName: String) = new File(fileName).getName.split('.')(0)
}
