package com.bsm.csv

import java.nio.file.Paths
import akka.stream.scaladsl.GraphDSL.Implicits.SourceArrow
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.stream.alpakka.csv.scaladsl.CsvParsing.SemiColon
import akka.stream.alpakka.csv.scaladsl.CsvToMap
import akka.stream.scaladsl.{FileIO, Flow, GraphDSL, RunnableGraph, Sink}
import akka.stream.{ActorMaterializer, ClosedShape, Materializer}
import akka.util.Timeout
import com.bsm.csv.actor.AckingReceiver
import com.bsm.csv.actor.AckingReceiver.Ack
import scalaz.Scalaz._
import scala.concurrent.duration.DurationInt

trait IdResolvable {

    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: Materializer = ActorMaterializer()
    implicit val askTimeout = Timeout(30 seconds)

    def idResolverActor(name: String) = system.actorOf(Props(new AckingReceiver(ackWith = Ack)), name)

    def resolve(references: List[String]) = {
        val idResolver = idResolverActor(fileNameWithoutExtension(references.head))
        val idResolverGraph = RunnableGraph.fromGraph(
            buildIdResolverGraph(references.head, references.tail, idResolver)
        )
        idResolverGraph.run()
        Thread.sleep(5000)
    }

    def buildIdResolverGraph(referencedFile: String,
                             codeFieldNames: List[String],
                             targetActor: ActorRef) = {
        val codes = if (codeFieldNames.isEmpty) List("code") else codeFieldNames
        val source = FileIO.fromPath(Paths.get(s"$referencedFile"))
        val transformer = getCSVParsingFlow(SemiColon)
            .via(CsvToMap.toMapAsStrings())
            .via(Flow[Map[String, String]].map(rowMap => {
                for {
                    id <- rowMap.get("id")
                    code <- codes.map(rowMap.get).sequence
                } yield (id, code.mkString("@"))
            }))

        // sent from actor to stream to "ack" processing of given element
        val AckMessage = AckingReceiver.Ack

        // sent from stream to actor to indicate start, end or failure of stream:
        val InitMessage = AckingReceiver.StreamInitialized
        val OnCompleteMessage = AckingReceiver.StreamCompleted
        val onErrorMessage = (ex: Throwable) ⇒ AckingReceiver.StreamFailure(ex)

        val sink = Sink.actorRefWithAck(targetActor, InitMessage, AckMessage, OnCompleteMessage, onErrorMessage)

        GraphDSL.create() { implicit builder =>

            source ~> transformer ~> sink

            ClosedShape
        }
    }
}
