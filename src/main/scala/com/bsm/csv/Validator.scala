package com.bsm.csv

import java.io.File

trait Validator { self: App =>

    def validate() = {

        if (args.length < 2) {
            throw new IllegalArgumentException(
                s"Usage: ${self.getClass.getCanonicalName} {input-file} {output-file} [optional reference-files]"
            )
        }

        val inputCsv = args(0)
        val outputCsv = args(1)
        val referenceCsvs = args.slice(2, args.length).toList.map(refCsv => {
            refCsv.split(',').toList
        })

        if (inputCsv != "--no-input" && !new File(s"$inputCsv").exists()) {
            throw new RuntimeException(s"There is no input csv for the transformer ${self.getClass.getCanonicalName}")
        }

        referenceCsvs.foreach({ refFile =>
            if (!new File(s"${refFile.head}").exists()) {
                throw new RuntimeException(s"There is no referenced csv ${refFile.head} for the transformer ${self.getClass.getCanonicalName}")
            }
        })

        (inputCsv, outputCsv, referenceCsvs)
    }
}
