package com.bsm.csv.actor

import akka.actor.Actor

class DuplicateChecker extends Actor {

    var set: Set[String] = Set.empty

    override def receive: Receive = {
        case elem: String =>
            var isDuplicated = true
            if (!set.contains(elem)) {
                set = set + elem
                isDuplicated = false
            }
            sender ! isDuplicated
    }
}