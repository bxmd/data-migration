package com.bsm.csv.actor

import akka.actor.{Actor, ActorLogging}
import com.bsm.csv.actor.AckingReceiver._

object AckingReceiver {

    case class GetId(code: String)

    case object Ack

    case object StreamInitialized
    case object StreamCompleted
    final case class StreamFailure(ex: Throwable)
}

class AckingReceiver(ackWith: Any) extends Actor with ActorLogging {

    var map: Map[String, String] = Map.empty

    override def receive: Receive = {
        case StreamInitialized ⇒
            log.info("Stream initialized!")
            sender() ! Ack // ack to allow the stream to proceed sending more elements
            context.become(loading)


        case GetId(code) => sender ! map.getOrElse(code, "")
    }

    def loading: Receive = {
        case el@Some((id: String, code: String)) =>
            log.info("Received mapping: {}", el)
            map = map.updated(code, id)
            sender() ! Ack // ack to allow the stream to proceed sending more elements

        case StreamCompleted ⇒
            log.info("Stream completed!")
            context.become(receive)

        case StreamFailure(ex) ⇒
            log.error(ex, "Stream failed!")
    }
}