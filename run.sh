#!/usr/bin/env bash

######################################################################################################################
#
# Usage: ./run.sh com.bsm.csv.ABCTransformer input-file-path output-file-path reference-data-1 reference-data-2 ..
#
# reference-data-n: <reference-csv-file><,field-name-1,field-name2..>
#    field-name(s): field names used as code field. Default will be 'code' if not specified
#
######################################################################################################################
sbt "runMain $@"