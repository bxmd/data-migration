#!/usr/bin/env bash

#####################################################################################################################
#
# Usage: ./runAll.sh /home/nhantran/inputfolder /home/nhantran/outputfolder
#
# First argument: input folder including csv files exported from FoxPro tables
# Second argument: output folder including csv files generated from running this run.sh file
#
# Need csv files exported from legacy system to make this script work
#
#####################################################################################################################
set -e

#sbt "runMain com.bsm.csv.transformer.RegionTransformer $1/dm_khuvuc.csv $2/regions.csv"
#sbt "runMain com.bsm.csv.transformer.CityTransformer $1/dm_tinh.csv $2/cities.csv $2/regions.csv"
#sbt "runMain com.bsm.csv.transformer.PartnerTransformer $1/dm_dnvt.csv $2/partners.csv $2/cities.csv"
#sbt "runMain com.bsm.csv.transformer.PartnershipTransformer $1/dm_dnvt.csv $2/partnerships.csv $2/partners.csv"
#sbt "runMain com.bsm.csv.transformer.RouteTransformer $1/dm_tuyen.csv $2/routes.csv $2/cities.csv"
#sbt "runMain com.bsm.csv.transformer.TicketBoothTransformer $1/dm_quayve.csv $2/ticket_booths.csv"
#sbt "runMain com.bsm.csv.transformer.TimetableTransformer $1/dm_giotai.csv $2/timetables.csv $2/ticket_booths.csv $2/partners.csv $2/routes.csv"
#sbt "runMain com.bsm.csv.transformer.VehicleModelTransformer $1/dm_soxe.csv $2/vehicle_models.csv"
#sbt "runMain com.bsm.csv.transformer.SeatTypeGenerator $2/seat_types.csv"
#############sbt "runMain com.bsm.csv.transformer.FareTypeTransformer --no-input $2/fare_types.csv $2/seat_types.csv"
#############sbt "runMain com.bsm.csv.transformer.VehicleTransformer $1/dm_soxe.csv $2/vehicles.csv $2/partners.csv $2/routes.csv $2/vehicle_models.csv $2/ticket_booths.csv $2/fare_types.csv $2/seat_types.csv"
#sbt "runMain com.bsm.csv.transformer.GateGenerator $2/gates.csv"
sbt "runMain com.bsm.csv.transformer.FareTransformer1 $1/dm_banggia.csv $2/fares.csv $2/partners.csv $2/routes.csv $2/cities.csv $2/fare_types.csv"
sbt "runMain com.bsm.csv.transformer.FareTransformer2 $1/dm_banggia.csv $2/fares2.csv $2/partners.csv $2/routes.csv $2/cities.csv $2/fare_types.csv"
sbt "runMain com.bsm.csv.transformer.FareTransformer3 $1/dm_banggia.csv $2/fares3.csv $2/partners.csv $2/routes.csv $2/cities.csv $2/fare_types.csv"
#sbt "runMain com.bsm.csv.transformer.RegisteredVehicleEntranceFeeTransformer $1/dm_giaquaben.csv $2/registered_vehicle_entrance_fees.csv $2/routes.csv $2/seat_types.csv"
#sbt "runMain com.bsm.csv.transformer.VehicleTypeGenerator $2/vehicle_types.csv"
#sbt "runMain com.bsm.csv.transformer.UnregisteredVehicleEntranceFeeTransformer $1/dm_giavanglai.csv $2/unregistered_vehicle_entrance_fees.csv $2/vehicle_types.csv"
#sbt "runMain com.bsm.csv.transformer.NightParkingFeeTransformer $1/dm_giadaudem.csv $2/night_parking_fees.csv $2/seat_types.csv"
#sbt "runMain com.bsm.csv.transformer.NightParkingFeeItemTransformer $1/dm_giadaudem.csv $2/night_parking_fee_items.csv $2/night_parking_fees.csv $2/seat_types.csv"
#sbt "runMain com.bsm.csv.transformer.TicketSaleCommissionFeeTransformer $1/dm_giadichvubanve.csv $2/ticket_sale_commission_fees.csv $2/routes.csv"
#sbt "runMain com.bsm.csv.transformer.MonthlyPaymentFeeTransformer $1/dm_khoanthang.csv $2/monthly_payment_fees.csv $2/partners.csv $2/routes.csv $2/seat_types.csv"
