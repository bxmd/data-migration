name := "data-migration"

version := "0.1"

scalaVersion := "2.12.6"

val alpakkaVersion = "0.20"
val scalazVersion = "7.2.25"

libraryDependencies ++= Seq(
    "com.lightbend.akka" %% "akka-stream-alpakka-file" % alpakkaVersion,
    "com.lightbend.akka" %% "akka-stream-alpakka-csv" % alpakkaVersion,
    "org.scalaz" %% "scalaz-core" % scalazVersion
)